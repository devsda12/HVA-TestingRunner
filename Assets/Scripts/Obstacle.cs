﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    #region Trigger methods
    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "Player") return;

        GameManager.LoseLife();
        Debug.Log(GameManager.HealthPoints);
    }
    #endregion
}
