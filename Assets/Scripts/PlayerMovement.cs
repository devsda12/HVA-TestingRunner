﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public enum Lane { Left, Middle, Right }

    #region Class variables
    public Rigidbody playerRB;
    public float movementSpeed = 1;
    public float jumpPower = 1;

    [Header("Lanes")]
    public float laneDividerDistance;
    public Lane currentLane = Lane.Middle;
    #endregion

    private void Start()
    {
        InputHandler.SpacePressed += OnSpace;
        InputHandler.LeftArrowPressed += OnLeftArrow;
        InputHandler.RightArrowPressed += OnRightArrow;
    }

    private void OnDestroy()
    {
        InputHandler.SpacePressed -= OnSpace;
        InputHandler.LeftArrowPressed -= OnLeftArrow;
        InputHandler.RightArrowPressed -= OnRightArrow;
    }

    // Update is called once per frame
    void Update()
    {
        //Always moving on the z-axis
        playerRB.MovePosition(new Vector3(transform.position.x, transform.position.y, transform.position.z + ((0.1f * movementSpeed) * GameManager.MovementSpeedModifier)));
        GameManager.playerPos = transform.position;
    }

    #region Input responses
    void OnSpace()
    {
        playerRB.AddForce(new Vector3(0, 1 * jumpPower, 0));
    }

    void OnLeftArrow()
    {
        if ((int)currentLane > 0) currentLane -= 1;
        PositionOnLane();
    }

    void OnRightArrow()
    {
        if ((int)currentLane < 2) currentLane += 1;
        PositionOnLane();
    }

    void PositionOnLane()
    {
        switch (currentLane)
        {
            case Lane.Left:
                transform.position = new Vector3(-6.66f, transform.position.y, transform.position.z);
                break;

            case Lane.Middle:
                transform.position = new Vector3(0, transform.position.y, transform.position.z);
                break;

            case Lane.Right:
                transform.position = new Vector3(6.66f, transform.position.y, transform.position.z);
                break;
        }
    }
    #endregion
}
