﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public static class GameManager
{
    #region Events
    public delegate void ActionHandle();

    public static ActionHandle onDeath;
    public static ActionHandle onWin;
    #endregion

    #region Class variables
    static float _healthPoints = 3;
    public static float HealthPoints
    {
        get
        {
            return _healthPoints;
        }
        set
        {
            _healthPoints = value;
            if (_healthPoints <= 0) Die();
        }
    }

    static float _movementSpeedModifier = 1;
    public static float MovementSpeedModifier { get { return _movementSpeedModifier; } }

    static float _collectibles = 0;
    public static float Collectibles { get { return _collectibles; } }

    public static Vector3 playerPos;
    public static float DistanceRun
    {
        get
        {
            return Mathf.Abs((Vector3.zero - playerPos).z);
        }
    }
    public static float Score
    {
        get
        {
            return _collectibles * 10 + DistanceRun * 0.5f;
        }
    }

    public static bool hasDied, hasWon;
    #endregion

    #region Methods
    public static void LoseLife()
    {
        HealthPoints -= 1;
        TempSlowDown();
    }
    public static void Die()
    {
        onDeath.Invoke();
        hasDied = true;
        Time.timeScale = 0;
    }

    public static void Win()
    {
        onWin.Invoke();
        hasWon = true;
        Time.timeScale = 0;
    }

    public static void TempSlowDown()
    {
        _movementSpeedModifier = 0.3f;
        ResetMovement();
    }

    static async void ResetMovement()
    {
        await Task.Run(() => Thread.Sleep(500));
        _movementSpeedModifier = 1;
    }

    public static void ResetStats()
    {
        _healthPoints = 3;
        _collectibles = 0;
        hasDied = false;
        hasWon = false;
    }

    public static void AddCollectible() => _collectibles += 1;
    #endregion
}
