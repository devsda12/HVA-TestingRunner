﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScorePanel : MonoBehaviour
{
    #region Class variables
    public TextMeshProUGUI score, distance, lives, collectibles;
    #endregion

    // Update is called once per frame
    void Update()
    {
        score.text = string.Format("{0}: {1}", "Score", GameManager.Score);
        distance.text = string.Format("{0}: {1}", "Distance", GameManager.DistanceRun);
        lives.text = string.Format("{0}: {1}", "Lives", GameManager.HealthPoints);
        collectibles.text = string.Format("{0}: {1}", "Collectibles", GameManager.Collectibles);
    }
}
