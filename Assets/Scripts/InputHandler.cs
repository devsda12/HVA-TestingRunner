﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    #region Events
    public delegate void InputHandle();

    public static event InputHandle SpacePressed;
    public static event InputHandle LeftArrowPressed;
    public static event InputHandle RightArrowPressed;
    public static event InputHandle EPressed;
    #endregion

    #region Event invokation methods
    public static void PressSpace() => SpacePressed.Invoke();
    public static void PressLeftArrow() => LeftArrowPressed.Invoke();
    public static void PressRightArrow() => RightArrowPressed.Invoke();
    public static void PressE() => EPressed.Invoke();
    #endregion

    #region Event invokation by keyboard
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) PressSpace();
        if (Input.GetKeyDown(KeyCode.LeftArrow)) PressLeftArrow();
        if (Input.GetKeyDown(KeyCode.RightArrow)) PressRightArrow();
        if (Input.GetKeyDown(KeyCode.E)) PressE();
    }
    #endregion
}
