﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMovement : MonoBehaviour
{
    public float addMultiplier = 1.5f;
    Vector3 origPos;
    private void Start()
    {
        origPos = transform.position;
    }
    private void Update()
    {
        transform.position = new Vector3(origPos.x + Mathf.Cos(Time.timeSinceLevelLoad) * addMultiplier, transform.position.y, transform.position.z);
    }
}
