﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    #region Class variables
    public GameObject deathScreen;
    public GameObject winScreen;
    #endregion

    #region Unity methods
    private void Start()
    {
        GameManager.onDeath += OnDeath;
        GameManager.onWin += OnWin;
        InputHandler.EPressed += EPressed;
    }

    private void OnDestroy()
    {
        GameManager.onDeath -= OnDeath;
        GameManager.onWin -= OnWin;
        InputHandler.EPressed -= EPressed;
    }
    #endregion

    #region Functional methods
    void OnDeath() => deathScreen.SetActive(true);

    void OnWin() => winScreen.SetActive(true);
    
    void EPressed()
    {
        if (!deathScreen.activeSelf && !winScreen.activeSelf) return;
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); //Restart
    }
    #endregion
}
