﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "Player") return;

        GameManager.AddCollectible();
        Destroy(gameObject);
    }
}
