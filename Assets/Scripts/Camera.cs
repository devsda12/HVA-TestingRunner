﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    #region Class variables
    public Transform player;
    public float movementSpeed = 3;
    public float zOffset = -5;
    #endregion

    // Update is called once per frame
    void FixedUpdate()
    {
        //Only follow on X and Z axis
        transform.position = Vector3.Lerp(transform.position, new Vector3(player.position.x, transform.position.y, player.position.z + zOffset), Time.deltaTime * movementSpeed);
    }
}
