﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class CountDownScreen : MonoBehaviour
{
    #region Class variables
    public TextMeshProUGUI cdText;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        GameManager.ResetStats();
        StartCountdown();
    }

    async void StartCountdown()
    {
        Time.timeScale = 0;
        await Task.Delay(1000);
        cdText.text = "2";
        await Task.Delay(1000);
        cdText.text = "1";
        await Task.Delay(1000);
        Destroy(gameObject);
        Time.timeScale = 1;
    }
}
