﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests
{
    public class JumpUtil
    {
        [SetUp]
        public void Setup()
        {
            SceneManager.LoadScene("SampleScene");
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator NoJump()
        {
            yield return new WaitForSeconds(1);
            InputHandler.PressRightArrow();
            yield return new WaitForSeconds(1);
            InputHandler.PressLeftArrow();
            yield return new WaitForSeconds(1.7f);
            InputHandler.PressRightArrow();

            yield return new WaitForSeconds(3.7f);
            InputHandler.PressLeftArrow();

            yield return new WaitForSeconds(2f);
            InputHandler.PressRightArrow();
            yield return new WaitForSeconds(1.4f);
            InputHandler.PressLeftArrow();
            yield return new WaitForSeconds(1.1f);
            InputHandler.PressRightArrow();
            yield return new WaitForSeconds(1f);
            InputHandler.PressLeftArrow();
            InputHandler.PressLeftArrow();

            yield return new WaitForSeconds(2.5f);
            InputHandler.PressRightArrow();

            yield return new WaitForSeconds(1.4f);
            InputHandler.PressLeftArrow();

            yield return new WaitForSeconds(3f);
            InputHandler.PressRightArrow();

            yield return new WaitForSeconds(3f);
            InputHandler.PressLeftArrow();

            yield return new WaitForSeconds(1.5f);
            InputHandler.PressRightArrow();

            yield return new WaitForSeconds(3f);
            InputHandler.PressLeftArrow();

            //Assertions
            yield return new WaitForSecondsRealtime(5);
            Assert.IsTrue(GameManager.hasWon); //Checking if has won
            Assert.NotZero(GameManager.HealthPoints);
            Assert.NotZero(GameManager.Collectibles);
            Assert.NotZero(GameManager.Score);
            Assert.NotZero(GameManager.DistanceRun);
            Debug.Log(string.Format("HP: {0}, CL: {1}, SR: {2}, DR: {3}", GameManager.HealthPoints, GameManager.Collectibles, GameManager.Score, GameManager.DistanceRun));
        }

        [UnityTest]
        public IEnumerator Combined()
        {
            yield return new WaitForSeconds(1);
            InputHandler.PressRightArrow();
            yield return new WaitForSeconds(2);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(4.5f);
            InputHandler.PressLeftArrow();

            yield return new WaitForSeconds(2);
            InputHandler.PressSpace();
            yield return new WaitForSeconds(2);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(1f);
            InputHandler.PressLeftArrow();

            yield return new WaitForSeconds(3.5f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(3.5f);
            InputHandler.PressRightArrow();

            yield return new WaitForSeconds(3.3f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(4.5f);
            InputHandler.PressLeftArrow();

            //Assertions
            yield return new WaitForSecondsRealtime(5);
            Assert.IsTrue(GameManager.hasWon); //Checking if has won
            Assert.NotZero(GameManager.HealthPoints);
            Assert.NotZero(GameManager.Collectibles);
            Assert.NotZero(GameManager.Score);
            Assert.NotZero(GameManager.DistanceRun);
            Debug.Log(string.Format("HP: {0}, CL: {1}, SR: {2}, DR: {3}", GameManager.HealthPoints, GameManager.Collectibles, GameManager.Score, GameManager.DistanceRun));
        }

        [UnityTest]
        public IEnumerator OnlyJump()
        {
            yield return new WaitForSeconds(1);
            InputHandler.PressSpace();
            yield return new WaitForSeconds(3.1f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(2.3f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(3f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(2.3f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(2.3f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(4.4f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(5f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(4.5f);
            InputHandler.PressSpace();

            //Assertions
            yield return new WaitForSecondsRealtime(5);
            Assert.IsTrue(GameManager.hasWon); //Checking if has won
            Assert.NotZero(GameManager.HealthPoints);
            Assert.NotZero(GameManager.Collectibles);
            Assert.NotZero(GameManager.Score);
            Assert.NotZero(GameManager.DistanceRun);
            Debug.Log(string.Format("HP: {0}, CL: {1}, SR: {2}, DR: {3}", GameManager.HealthPoints, GameManager.Collectibles, GameManager.Score, GameManager.DistanceRun));
        }
    }
}
