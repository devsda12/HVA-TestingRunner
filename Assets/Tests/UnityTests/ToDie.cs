﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests
{
    public class ToDie
    {
        [SetUp]
        public void Setup()
        {
            SceneManager.LoadScene("SampleScene");
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator DieBeginning()
        {
            //Assertions
            yield return new WaitForSecondsRealtime(15);
            Assert.IsTrue(GameManager.hasDied); //Checking if has died
            Assert.Zero(GameManager.HealthPoints);
            Assert.Zero(GameManager.Collectibles);
            Assert.NotZero(GameManager.Score);
            Assert.NotZero(GameManager.DistanceRun);
            Debug.Log(string.Format("HP: {0}, CL: {1}, SR: {2}, DR: {3}", GameManager.HealthPoints, GameManager.Collectibles, GameManager.Score, GameManager.DistanceRun));
        }

        [UnityTest]
        public IEnumerator DieMiddle()
        {
            yield return new WaitForSeconds(1);
            InputHandler.PressRightArrow();
            yield return new WaitForSeconds(2);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(4.5f);
            InputHandler.PressLeftArrow();

            yield return new WaitForSeconds(2);
            InputHandler.PressSpace();
            yield return new WaitForSeconds(2);
            InputHandler.PressSpace();

            //Assertions
            yield return new WaitForSecondsRealtime(12);
            Assert.IsTrue(GameManager.hasDied); //Checking if has died
            Assert.Zero(GameManager.HealthPoints);
            Assert.NotZero(GameManager.Collectibles);
            Assert.NotZero(GameManager.Score);
            Assert.NotZero(GameManager.DistanceRun);
            Debug.Log(string.Format("HP: {0}, CL: {1}, SR: {2}, DR: {3}", GameManager.HealthPoints, GameManager.Collectibles, GameManager.Score, GameManager.DistanceRun));
        }

        [UnityTest]
        public IEnumerator DontDie()
        {
            yield return new WaitForSeconds(1);
            InputHandler.PressRightArrow();
            yield return new WaitForSeconds(2);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(4.5f);
            InputHandler.PressLeftArrow();

            yield return new WaitForSeconds(2);
            InputHandler.PressSpace();
            yield return new WaitForSeconds(2);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(1f);
            InputHandler.PressLeftArrow();

            yield return new WaitForSeconds(3.5f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(3.5f);
            InputHandler.PressRightArrow();

            yield return new WaitForSeconds(3.3f);
            InputHandler.PressSpace();

            yield return new WaitForSeconds(4.5f);
            InputHandler.PressLeftArrow();

            //Assertions
            yield return new WaitForSecondsRealtime(5);
            Assert.IsTrue(GameManager.hasWon); //Checking if has won
            Assert.NotZero(GameManager.HealthPoints);
            Assert.NotZero(GameManager.Collectibles);
            Assert.NotZero(GameManager.Score);
            Assert.NotZero(GameManager.DistanceRun);
            Debug.Log(string.Format("HP: {0}, CL: {1}, SR: {2}, DR: {3}", GameManager.HealthPoints, GameManager.Collectibles, GameManager.Score, GameManager.DistanceRun));
        }
    }
}
